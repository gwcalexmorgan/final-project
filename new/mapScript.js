
		function myFunction() {
			d3.select("#linesdrawn").remove();
		}
	
		function myFunction1() {
			alert("here");
			alert (fileinput.files.length);
			for (var i = 0; i < fileinput.files.length; i++){	
				d3.select("#linesdrawn").remove();
			}
		}
		
		var width = 1000,
			height = 600,
			scale = (width - 1) / 2 / Math.PI;

		var projection = d3.geo.equirectangular();

		var zoom = d3.behavior.zoom()
			.translate([width / 2, height / 2])
			.scale(scale)
			.scaleExtent([scale / 5, 10 * scale])
			.on("zoom", zoomed);

		var path = d3.geo.path()
			.projection(projection);

		var svg = d3.select("body").append("svg")
			.attr("width", width)
			.attr("height", height)
			.attr("float","left");
		  //.append("g");

		//var g = svg.append("g");

				var tip = d3.tip()
				  .attr('class', 'd3-tip')
				  .offset([-10, 0])
				  .html(function(d) {
				  if (d['name'] == 'Blue Whale'){
					return d['tip'];
				  }
				  if (d['name'] == 'Fin Whale'){
					return d['tip'];
				  }
				  if (d['name'] == 'Sperm Whale'){
					return d['tip'];
				  }
				  if (d['name'] == 'Humpback Whale'){
					return d['tip'];
				  }
				  if (d['name'] == 'Shortfin Mako Shark'){
					return d['tip'];
				  }
				  if (d['name'] == 'Southern Elephant Seal'){
					return d['tip'];
				  }
				  if (d['name'] == 'Leatherback Sea Turtle'){
					return d['tip'];
				  }
				  if (d['name'] == 'Loggerhead Sea Turtle'){
					return d['tip'];
				  }
				  if (d['name'] == 'Black-footed Albatross'){
					return d['tip'];
				  }
				  if (d['name'] == 'Salmon Shark'){
					return d['tip'];
				  }
				  if (d['name'] == 'Northern Elephant Seal'){
					return d['tip'];
				  }
				  if (d['name'] == 'Blue Shark'){
					return d['tip'];
				  }
				  if (d['name'] == 'Common Thresher Shark'){
					return d['tip'];
				  }
				  });

				svg.append("rect")
					.attr("class", "overlay")
					.attr("width", width)
					.attr("height", height);

				svg .call(zoom)
					.call(zoom.event)
					.call(tip);

		var lines = 0

				d3.json("world-110m.json", function(error, world) {
				  if (error) throw error;

				  svg.append("path")
					  .datum({type: "Sphere"})
					  .attr("class", "sphere")
					  .attr("d", path);

				  svg.append("path")
					  .datum(topojson.merge(world, world.objects.countries.geometries))
					  .attr("class", "land")
					  .attr("d", path)

				  svg.append("path")
					  .datum(topojson.mesh(world, world.objects.countries, function(a, b) { return a !== b; }))
					  .attr("class", "boundary")
					  .attr("d", path);
				  lines = topojson.mesh(world, world.objects.countries, function(a, b) { return a !== b; })
				});


		function drawLines(data) {
			  var animalPath = {"name":data['name'], "id":data['id'], "type": "MultiLineString", "coordinates":[[]]}
			  for (var i = 0; i < data['locations'].length; i++){
				animalPath["coordinates"][0].push([data['locations'][i]['latitude'], data['locations'][i]['longitude']])
			  }
		  lines = animalPath;
	  if (animalPath.name=== "Blue Whale"){
		  animalPath['tip'] = "Blue Whale" + "<br>" + "<img src=" + "https://s-media-cache-ak0.pinimg.com/236x/d1/be/51/d1be518c408370f0f06f156fd1e89a04.jpg" + ">" + "<br>" + "The Blue Whale is a type of baleen whale.</br> It's also the largest known animal on Earth!</br> These huge marine mammals can weigh</br> up to 200 tons and grow up to 30 meters </br>long. Sadly, blue whales are hunted </br>extensively for their large quantities</br> of baleen, blubber, and meat. In the United</br> States, it's illegal to hunt a blue whales.</br> Worldwide, however, but this isn't the</br> case. Blue whales may also be harmed</br> by pollution, ship strikes, or entanglement </br> in fishing gear.";
				svg.append("path")
				.datum(animalPath)
				.attr("class", "line")
				.attr("stroke", "#5882FA")
				.attr("d", path)
				.attr("id","linesdrawn")
				.on("mouseover", tip.show)
				.on("mouseout", tip.hide);
			}
		else if(animalPath.name=== "Fin Whale"){
			animalPath['tip'] = "Fin Whale" + "<br>" + "<img src=" + "http://bocktherobber.com.cdn.ie/wordpress/wp-content/uploads/thumb-cache/whale-5bb00045f40b9d21af5f6978d70722c2-200x120-100-nocrop.jpg" + ">" + "<br>" + "Fin Whales are the second-largest<br> species of whale, with a maximum</br> length of about 75 feet (22 m) in </br>the Northern Hemisphere, and 85 feet</br> (26 m) in theSouthern Hemisphere. Fin</br> Whales are severely impacted by hunters</br> who profit from selling whale parts.</br> This is called commercial whaling.";
				svg.append("path")
				.datum(animalPath)
				.attr("class", "line")
				.attr("stroke", "#0431B4")
				.attr("d", path)
				.attr("id","linesdrawn")
				.on("mouseover", tip.show)
				.on("mouseout", tip.hide);
		}
		else if(animalPath.name=== "Sperm Whale"){
			animalPath['tip'] = "Sperm Whale" + "<br>" + "<img src=" + "http://d3819ii77zvwic.cloudfront.net/wp-content/uploads/2015/03/sperm-whale-300x142.jpg" + ">" + "<br>" + "The Sperm Whale is a large toothed whale that is</br> part of the cetacean species, which includes all</br> species of whale, dolphin and porpoise. Sperm</br> Whales body parts are used for various products and </br>goods, like oil, blubber and meat. There are still </br>a few countries that hunt sperm whales today. ";
				svg.append("path")
				.datum(animalPath)
				.attr("class", "line")
				.attr("stroke", "#A9D0F5")
				.attr("d", path)
				.attr("id","linesdrawn")
				.on("mouseover", tip.show)
				.on("mouseout", tip.hide);
		}
		else if(animalPath.name=== "Humpback Whale"){
			animalPath['tip'] = "Humpback Whale" + "<br>" + "<img src=" + "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcSaJwnzW8_p66jt-aiFwN5U-5R5qd5O4j-jDVU_KQjiw2IzVcTU" + ">" + "<br>" + "Humpback Whale were considered</br> an endangered species due to </br>hunters. Today it is being debated</br> whether to take them off the</br> endangered animal list.";
				svg.append("path")
				.datum(animalPath)
				.attr("class", "line")
				.attr("stroke", "#2ECCFA")
				.attr("d", path)
				.attr("id","linesdrawn")
				.on("mouseover", tip.show)
				.on("mouseout", tip.hide);
		}
		else if(animalPath.name=== "Shortfin Mako Shark"){
			animalPath['tip'] = "Shortfin Mako Shark" + "<br>" + "<img src=" + "http://www.gliscomunicati.it//public/users/apokalipse/image/Verdesca.jpg" + ">" + "<br>" + "The Shortfin Mako is believed to be the fastest</br> of all sharks. It's capable of reaching speeds of</br> up to 35 kilometres per hour. These quick</br> swimmers are killed for their fins, which serve</br> medicinal purposes.";
				svg.append("path")
				.datum(animalPath)
				.attr("class", "line")
				.attr("stroke", "#00CC00")
				.attr("d", path)
				.attr("id","linesdrawn")
				.on("mouseover", tip.show)
				.on("mouseout", tip.hide);
		}
		else if(animalPath.name=== "Southern Elephant Seal"){
			animalPath['tip'] = "Southern Elephant Seal" + "<br>" + "<img src=" + "https://s-media-cache-ak0.pinimg.com/236x/2d/96/8b/2d968bad96466f5d10260020f9cb832e.jpg" + ">" + "<br>" + "The southern elephant seal is one of the</br> two extant species of elephant seals. Males</br> can weigh up to eight to ten times as much </br>as females. It is both the largest pinniped</br> and member of the order Carnivora living </br>today, as well as the largest Antarctic seal.</br> Elephant seals were aggressively hunted</br> for their oil.";
				svg.append("path")
				.datum(animalPath)
				.attr("class", "line")
				.attr("stroke", "#FFFF00")
				.attr("d", path)
				.attr("id","linesdrawn")
				.on("mouseover", tip.show)
				.on("mouseout", tip.hide);
		}
		else if(animalPath.name=== "Leatherback Sea Turtle"){
			animalPath['tip'] = "Leatherback Sea Turtle" + "<br>" + "<img src=" + "http://media-cache-ec0.pinimg.com/237x/a4/8c/79/a48c799045259e2e91cb34306592bf6b.jpg" + ">" + "<br>" + "Leather back turtles belong to the</br> Dermochelyidae family. They are more</br> than six feet long, weighing as much as</br> 1400 pounds (636 kg), leather backs are the</br>  world's largest pelagic(ocean-going) turtles.</br> Leather backs build their nests on remote</br> stretches of sandy beach. Loss of these</br> coastal nesting habitats is one of the</br> primary threats to leatherback survival.</br> The turtle is hunted for its oil and flesh.";
				svg.append("path")
				.datum(animalPath)
				.attr("class", "line")
				.attr("stroke", "#CC00CC")
				.attr("d", path)
				.attr("id","linesdrawn")
				.on("mouseover", tip.show)
				.on("mouseout", tip.hide);
		}
		else if(animalPath.name=== "Loggerhead Sea Turtle"){
			animalPath['tip'] = "Loggerhead Sea Turtle" + "<br>" + "<img src=" + "http://upload.wikimedia.org/wikipedia/commons/thumb/8/81/Loggerhead_Sea_Turtle_(Caretta_caretta)_2.jpg/220px-Loggerhead_Sea_Turtle_(Caretta_caretta)_2.jpg" + ">" + "<br>" + "The loggerhead sea turtle is an oceanic</br> turtle distributed throughout the world.</br> It is a marine reptile, belonging to</br> the family Cheloniidae. Loggerheads in</br> the southeastern U.S. weigh an average</br> of 250 lbs (113 kg) and are generally</br> about 36 inches (92 cm) long. Untended</br> fishing gear is responsible for many</br> loggerhead deaths. Turtles may also</br> suffocate if they are trapped in fishing</br> trawls.";
				svg.append("path")
				.datum(animalPath)
				.attr("class", "line")
				.attr("stroke", "#FF0066")
				.attr("d", path)
				.attr("id","linesdrawn")
				.on("mouseover", tip.show)
				.on("mouseout", tip.hide);
		}
		else if(animalPath.name=== "Black-footed Albatross"){
			animalPath['tip'] = "Black-footed Albatross" + "<br>" + "<img src=" + "http://cs409328.vk.me/v409328031/c6e/kSGGStAQjFc.jpg" + ">" + "<br>" + "The Black-footed albatross is</br> a large sea bird that belongs to the </br>albatross family Diomedeidae. Many</br> albatross species are in trouble and</br> need ourhelp. Commercial fishing</br> practices are considered the greatest threat</br> to the survival of many albatross species.</br> Other threats include loss of habitat,</br>introduced predators, eating or becoming</br> tangled up in plastic, oil spills and</br> climate change. ";
				svg.append("path")
				.datum(animalPath)
				.attr("class", "line")
				.attr("stroke", "#FF6600")
				.attr("d", path)
				.attr("id","linesdrawn")
				.on("mouseover", tip.show)
				.on("mouseout", tip.hide);
		}

		else if(animalPath.name=== "Salmon Shark"){
			animalPath['tip'] = "Salmon Shark" + "<br>" + "<img src=" + "https://s-media-cache-ak0.pinimg.com/236x/f3/e1/fd/f3e1fdea26664a34fdcf2c36ae68fc48.jpg" + ">" + "<br>" + "The salmon shark is from the Lamnidae</br> family. Salmon Shark have an</br> average size is between 250 and 280cm </br>long, with a maximum size of about 300</br> cm. Their populations could be threatened</br> because they are fished by Japanese longline</br> fishermen in the northern Pacific.";
				svg.append("path")
				.datum(animalPath)
				.attr("class", "line")
				.attr("stroke", "#009933")
				.attr("d", path)
				.attr("id","linesdrawn")
				.on("mouseover", tip.show)
				.on("mouseout", tip.hide);
		}
		else if(animalPath.name=== "Northern Elephant Seal"){
			animalPath['tip'] = "Northern Elephant Seal" + "<br>" + "<img src=" + "https://s-media-cache-ak0.pinimg.com/236x/e8/08/35/e808358cc467be58c8709b80a7102f16.jpg" + ">" + "<br>" + "The northern elephant seal is one of the</br> two extant species of elephant seals. Males</br> can weigh up to eight to ten times as much </br>as females. It is both the largest pinniped</br> and member of the order Carnivora living </br>today, as well as the largest Antarctic seal.</br> Elephant seals were aggressively hunted</br> for their oil.";
				svg.append("path")
				.datum(animalPath)
				.attr("class", "line")
				.attr("stroke", "#FFFF66")
				.attr("d", path)
				.attr("id","linesdrawn")
				.on("mouseover", tip.show)
				.on("mouseout", tip.hide);
		}
		else if(animalPath.name=== "Blue Shark"){
			animalPath['tip'] = "Blue Shark" + "<br>" + "<img src=" + "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcQrmSZXd7JQkdX3wbe0bdYQuD4WCDGq5w7UIGEGgmyP2AfjY5SZTg" + ">" + "</br>" + "Blue shark will grow to be 12.5 feet</br> in length and adult blue whales weight</br> between 70 to 120 pounds. Millions</br> of Blue sharks are hunted each year.</br> They are killed to use their body</br> for various types of products, the</br> skin is dried out to make leather</br> out of, and the liver is used to remove</br> oil from.";
				svg.append("path")
				.datum(animalPath)
				.attr("class", "line")
				.attr("stroke", "#006600")
				.attr("d", path)
				.attr("id","linesdrawn")
				.on("mouseover", tip.show)
				.on("mouseout", tip.hide);
		}
		else if(animalPath.name=== "Common Thresher Shark"){
			animalPath['tip'] = "Common Thresher Shark" + "<br>" + "<img src=" + "http://cdn.shopify.com/s/files/1/0122/5342/files/thintail_thresher_shark_fs_medium.jpg?1163" + ">" + "</br>" + "The Common Thresher Shark is the</br> largest species of thresher shark</br> belonging to the family of Alopiidae,</br> reaching some 6 m (20 ft) in length</br> and weight weigh 348 kg on average and</br> can reach up to 500 kg. Common</br> Thresher is distributed worldwide in tropical</br> and temperate waters, though it prefers</br> cooler temperatures. Common Thresher sharks</br> are hunted for their meat, for</br> their liver oil, skin (for leather),</br> and their fins, for use in shark-fin soup.";
				svg.append("path")
				.datum(animalPath)
				.attr("class", "line")
				.attr("stroke", "#66FF66")
				.attr("d", path)
				.attr("id","linesdrawn")
				.on("mouseover", tip.show)
				.on("mouseout", tip.hide);
		}
		else{
				svg.append("path")
				.datum(animalPath)
				.attr("class", "line")
				.attr("stroke", "red")
				.attr("d", path)
				.attr("id","linesdrawn")
				.on("mouseover", tip.show)
				.on("mouseout", tip.hide);
		}
		}

		function zoomed() {
			projection
				  .translate(zoom.translate())
				  .scale(zoom.scale());

			  svg.selectAll("path")
				  .attr("d", path);
			}

			d3.select(self.frameElement).style("height", height + "px");

