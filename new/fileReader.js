var animalData = [] // Object with all animal data

/*
 * Creates an animal object from file data
 */
function addNewAnimal(file) {
  if (file) { // Does file actually exist?
    var r = new FileReader(); // Create file reader
    r.onload = function(e) { // Wait for file to load before reading
      var data = {'name':'', 'id':'', 'locations':[]} // New animal data

      fileContents = r.result.split("\n"); // Split on every newline
      data['name'] = fileContents[0].trim(); // First Line is the animal name
      data['id'] = fileContents[1].trim(); // Second Line is the animal id + ?

      for (var i = 2; i < fileContents.length; i++) { // For every line after
        contents = fileContents[i].split(","); // Split line contents on commas
        if (contents.length >= 4){ // Prevents keyerror
          data['locations'].push({
            'date':contents[0].trim(),
            'time':contents[1].trim(),
            'longitude':contents[2].trim(),
            'latitude':contents[3].trim()
          })
        }
      }
      animalData.push(data); // add animal to json
      drawLines(data);
    }
    r.readAsText(file);
  } else { 
    throw "Failed to load file"; // If fails, throw error to console
  }
}

function GetFileData(){
  var fileinput = document.getElementById("fileItem"); // Get files from selector

  if ('files' in fileinput) {
    if (fileinput.files.length == 0) {
      throw "No files selected";
    }
    else {
      for (var i = 0; i < fileinput.files.length; i++) { 
        try {
          addNewAnimal(fileinput.files[i]); // For each file, create animal data
        } catch (err) {
          throw "Failed to read file";
        }
      }
    }
  }
}

