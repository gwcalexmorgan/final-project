

$( document ).ready(function() {
		//The data for our line
	//var longitude = [1,20,40,60,80,100];
	//var latitude = [5,20,10,40,5,60];	
	
	//var l = 0
	
	var lineData = [ { "x": 1, "y": 5},
					 { "x": 20,  "y": 20},
					 { "x": 40,  "y": 10}, 
					 { "x": 60,  "y": 40},
					 { "x": 80,  "y": 5},  
					 { "x": 100, "y": 60},
					 { "x": 500,  "y": 5} ];

	//This is the accessor function we talked about above
	var lineFunction = d3.svg.line()
							 .x(function(d) { return d.x; })
							 .y(function(d) { return d.y; })
							 .interpolate("linear");

	//The SVG Container
	var svgContainer = d3.select("body").append("svg")
										.attr("width", 200)
										.attr("height", 200);

	//The line SVG Path we draw
	var lineGraph = svgContainer.append("path")
								.attr("d", lineFunction(lineData))
								.attr("stroke", "blue")
								.attr("stroke-width", 2)
								.attr("fill", "none");

	//alert(document.documentElement.innerHTML);
});