

$(document).ready(function(){
	
	// CODE FOR GLOBE
	var width = 620;
	var height = 600;
	
	//make svg element
	var svg = d3.select("#globe").append("svg")
		.attr("width", width)
		.attr("height", height);	
		
	//make projection (globe)
	var projection = d3.geo.orthographic()
		 .scale(300)
		 //.clipAngle(90)
		 .translate([width/2, height/2])
		;
	
	//make path
	var path = d3.geo.path()
		.projection(projection);
	//make graticule(lines)
	var graticule = d3.geo.graticule();
	

	svg.append("path")
		.datum(graticule)
		.attr("class","graticule")
		.attr("d",path);
		
	
	// END CREATE GLOBE
		
	// MOVE GLOBE
		var speed = 100; 
		var c = 0;
		
		function move(){
			c += 0.2;
			projection.rotate([c, 0,0]);
			   svg.selectAll("path").attr("d", path);
			  svg.selectAll("circle").remove(); // REMOVES EVERY DOT EVER 
			var path2 = d3.geo.path()
				.projection(projection)
				 svg.selectAll(".pin")
			  .data(animalData)
			  .enter().append("circle", ".pin")
			  .attr("r", 5)
				.attr("transform", function(d) {
					return "translate(" + projection([
					  d.locations.longitude,
					  d.locations.latitude
					]) + ")";
				  });
		
				}

		 setInterval(move, speed); 
	//END MOVE GLOBE

		
/* 	var places = [
	  {
		name: "Wollongong, Australia",
		location: {
		  latitude: 75,
		  longitude: 43,
		  coordinates: [43,75]
		}
	  },
	  {
		name: "bw",
		location: {
		  latitude: 38.222,
		  longitude: 123.663,
		  coordinates: [123.663,38.222]
		}
	  },
	  {
		name: "bw2",
		location: {
		  latitude: 0,
		  longitude: 50,
		  coordinates: [50,0]
		}
	  },
	  {
		name: "Newcastle, Australia",
		location: {
		  latitude: 32.92669,
		  longitude: 151.77892,
		  coordinates: [151.77892,32.92669]
		}
	  }
	]; */
	
	
/* 	//for (var i = 0; i < places.length; i++){
		
	//	var x = places[i].location.longitude;
	//	var y = places[i].location.latitude;
	

	//This is the accessor function we talked about above
	var lineFunction = d3.svg.line()
							.x(function(d) { return d.location.longitude; })
							.y(function(d) { return d.location.latitude; })
							 .interpolate("linear");

	//The SVG Container
	var svgContainer = d3.select("body").append("svg")
										.attr("width", 620)
										.attr("height", 600);

	//The line SVG Path we draw
	var lineGraph = svgContainer.append("path")
								.attr("d", lineFunction(places))
								.attr("stroke", "blue")
								.attr("stroke-width", 2)
								.attr("fill", "none");
	//}		
	 */
	var swoosh = d3.svg.line()
      .x(function(d) { return d[0] })
      .y(function(d) { return d[1] })
      .interpolate("cardinal")
      .tension(.0); 
	  
	var sky = d3.geo.orthographic()
    .translate([width / 2, height / 2])
    .clipAngle(90)
    .scale(300);
	
	var proj = d3.geo.orthographic()
    .translate([width / 2, height / 2])
    .clipAngle(90)
    .scale(220);

	var links = [];
	var arcLines = [];
	
	animalData.forEach(function(a) {
		animalData.forEach(function(b) {
		  if (a !== b) {
			links.push({
			  source: a.locations.coordinates,
			  target: b.locations.coordinates
			});
		  }
		});
	  });

	// build geoJSON features from links array
/* 	links.forEach(function(e,i,a) {
		var feature =   {"location": {"coordinates": [e.source,e.target] }}
		arcLines.push(feature)
	})

	  svg.append("g").attr("class","arcs")
		.selectAll("path").data(arcLines)
		.enter().append("path")
		  .attr("class","arc")
		  .attr("d",path)

	  svg.append("g").attr("class","flyers")
		.selectAll("path").data(links)
		.enter().append("path")
		.attr("class","flyer")
		.attr("d", function(d) { return swoosh(flying_arc(d)) })

	  refresh();


	function flying_arc(pts) {
 	  var source = pts.source,
		  target = pts.target; 

	  var mid = location_along_arc(source, target, .5);
	  var result = [ proj(source),
					 sky(mid),
					 proj(target) ]
	  return result;
	}



	function refresh() {
	  svg.selectAll(".land").attr("d", path);
	  svg.selectAll(".point").attr("d", path);
	  
	  svg.selectAll(".arc").attr("d", path)
		.attr("opacity", function(d) {
			return fade_at_edge(d)
		})

	  svg.selectAll(".flyer")
		.attr("d", function(d) { return swoosh(flying_arc(d)) })
		.attr("opacity", function(d) {
		  return fade_at_edge(d)
		}) 
	}

	function fade_at_edge(d) {
	  var centerPos = proj.invert([width/2,height/2]),
		  arc = d3.geo.greatArc(),
		  start, end;
	  // function is called on 2 different data structures..
	  if (d.source) {
		start = d.source, 
		end = d.target;  
	  }
	  else {
		start = d.locations.coordinates[0];
		end = d.locations.coordinates[1];
	  }
	  
	  var start_dist = 1.57 - arc.distance({source: start, target: centerPos}),
		  end_dist = 1.57 - arc.distance({source: end, target: centerPos});
		
	  var fade = d3.scale.linear().domain([-.1,0]).range([0,.1]) 
	  var dist = start_dist < end_dist ? start_dist : end_dist; 

	  return fade(dist)
	}

	function location_along_arc(start, end, loc) {
	  var interpolator = d3.geo.interpolate(start,end);
	  return interpolator(loc)
	} */


});	

